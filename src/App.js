import React from 'react'
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Footer from './components/Footer/Footer';
import Clientslist from './components/Clientslist/Clientslist';
import Clientsheader from './components/Clientslist/Clientsheader';

function App() {
  return (
    <div>
      <Navbar />
      <Clientsheader />
      <Clientslist />
      <Footer />
    </div>
  )
}

export default App
