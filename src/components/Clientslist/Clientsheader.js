import React from 'react'
import './Clients.css';



function Clientsheader() {

    
    return (
        <div className="container clientsheader">
            <div class="row">
                <div class="col-md-4">
                    <div className="title">
                        <h1>Minu kliendid</h1>
                    </div>
                    <div className="breadcrumbs">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page">Minu kliendid</li>
                        </ol>
                    </nav>
                    </div>
                </div>
                <div class="col-md-4 offset-md-4">
                <button
                    type="button"
                    class="btn btn-primary"
                    data-mdb-toggle="modal"
                    data-mdb-target="#exampleModal"
                    >
                    Loo uus klient<i class="fas fa-plus"></i>
                </button>


                <div
                class="modal fade"
                id="exampleModal"
                tabindex="-1"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
                >
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><i class="far fa-user-circle"></i>Loo uus klient</h5>
                            <button
                            type="button"
                            class="btn-close"
                            data-mdb-dismiss="modal"
                            aria-label="Close"
                            ></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-outline">
                                <input type="text" id="typeText" class="form-control" />
                                <label class="form-label" for="typeText">Text input</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                            Tagasi
                            </button>
                            <button type="button" class="btn btn-primary">Edasi</button>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Clientsheader
