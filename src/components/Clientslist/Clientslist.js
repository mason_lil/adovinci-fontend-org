import React from 'react'
import * as mdb from 'mdb-ui-kit';
import './Clients.css';

function Clientslist() {
    return (
        <div className="container shadow-box">
            <div className="row">
                <div className="col">
                    <h1 className="text-center">Ühtegi klienti pole lisatud</h1>
                    <p className="text-center">Klientide haldamiseks lisas uus klient, vali meediaomanikud ja määra formaadid ja allahindulused</p>
                </div>
            </div>
        </div>
    )
}

export default Clientslist
