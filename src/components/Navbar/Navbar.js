import React from 'react'
import * as mdb from 'mdb-ui-kit';
import './Nav.css';

export default function Navbar() {
    return (
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <button
      class="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="fas fa-bars"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <a class="navbar-brand mt-2 mt-lg-0" href="#">
        <img/>
      </a>
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="#">Asukohad</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Kampaaniad</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Kavandid</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Kliendid</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Blogi</a>
        </li>
      </ul>
    </div>
    <div class="d-flex align-items-center">
      <a class="text-reset me-3" href="#">
        <i class="fas fa-globe"></i>
      </a>

      <a
        class="text-reset me-3 dropdown-toggle hidden-arrow"
        href="#"
        id="navbarDropdownMenuLink"
        role="button"
        data-mdb-toggle="dropdown"
        aria-expanded="false"
      >
        <i class="far fa-comment"></i>
        <span class="badge rounded-pill badge-notification bg-danger">12</span>
      </a>
      <ul
        class="dropdown-menu dropdown-menu-end"
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <a class="dropdown-item" href="#">Some news</a>
        </li>
        <li>
          <a class="dropdown-item" href="#">Another news</a>
        </li>
        <li>
          <a class="dropdown-item" href="#">Something else here</a>
        </li>
      </ul>

      <a
        class="dropdown-toggle d-flex align-items-center hidden-arrow"
        href="#"
        id="navbarDropdownMenuLink"
        role="button"
        data-mdb-toggle="dropdown"
        aria-expanded="false"
      >
          <div className="info">
              <h4>Mart Kingisepp</h4>
              <p>Adocinci</p>
          </div>
        <img
          src="https://mdbootstrap.com/img/new/avatars/2.jpg"
          class="rounded-circle"
          height="32"
          alt=""
          loading="lazy"
        />
        <i class="fas fa-chevron-down"></i>
      </a>
      <ul
        class="dropdown-menu dropdown-menu-end"
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <a class="dropdown-item" href="#">My profile</a>
        </li>
        <li>
          <a class="dropdown-item" href="#">Settings</a>
        </li>
        <li>
          <a class="dropdown-item" href="#">Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

    )
}
