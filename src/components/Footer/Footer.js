import React from 'react'
import './Footer.css';
import * as mdb from 'mdb-ui-kit';

export default function Footer() {
    return (

<footer class="text-center text-lg-start bg-light text-muted">
  <section class="">
    <div class="container text-center text-md-start mt-5">
      <div class="row mt-3">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <h6 class="fw-bold mb-4">
                Minu Adovinci
          </h6>
          <p>
            <a href="#!" class="text-reset">Minu kampaaniad</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Minu kavandid</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Minu kliendid</a>
          </p>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <h6 class="fw-bold mb-4">
            Asukohad
          </h6>
          <p>
            <a href="#!" class="text-reset">LED ja LCD ekraanid</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Suured tahvlid ja Seinad</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Bussiootepaviljonid</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Valgusvitriinid</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Piilarid ja Prügikastid</a>
          </p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          <h6 class="fw-bold mb-4">
            Kategooriad
          </h6>
          <p>
            <a href="#!" class="text-reset">Digitaalne</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Print</a>
          </p>
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <p>Valukoja 8, 11415 Tallinn</p>
          <p>+37253422430</p>
          <p>info@adovinci.com</p>
        </div>
      </div>

    </div>
  </section>
</footer>

    )
}
